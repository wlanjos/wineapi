package br.com.wine.api.controller;

import br.com.wine.api.service.LojaService;
import br.com.wine.api.dto.LojaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/loja")
public class LojaController {

    @Autowired
    LojaService service;

    @PostMapping
    public ResponseEntity<LojaDTO> create(@RequestBody LojaDTO lojaDTO){
        return ResponseEntity.ok().body(service.save(lojaDTO));
    }

    @GetMapping
    public ResponseEntity<LojaDTO> buscarLoja(@RequestParam String codigoloja){
        LojaDTO loja = new LojaDTO();
        
        loja = service.findByCodigoLoja(codigoloja);

        return (loja == null) ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(loja);
    }
}
