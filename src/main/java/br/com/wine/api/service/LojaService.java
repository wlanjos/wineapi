package br.com.wine.api.service;


import br.com.wine.api.dto.Cep;
import br.com.wine.api.dto.LojaDTO;
import br.com.wine.api.repository.CEPRepository;
import br.com.wine.api.repository.LojaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class LojaService {

    @Autowired
    private LojaRepository repo;

    @Autowired
    private CEPRepository cepRepository;





    public LojaDTO save(LojaDTO lojaDTO) {

        LojaDTO loja = new LojaDTO();

        loja = repo.save(lojaDTO);
        Cep cep =  new Cep();



        int iteration = lojaDTO.getFaixaInicio();

        for (int i = lojaDTO.getFaixaInicio(); i < lojaDTO.getFaixaFim(); i++) {
            cep.setLoja(loja);
            cep.setCep(iteration);

            System.out.println(iteration);

            cepRepository.save(cep);
            iteration++;
        }
        return loja;
    }

    public LojaDTO findByCodigoLoja(String codigo){
        return repo.findByCodigoLoja(codigo);
    }


}
