package br.com.wine.api.repository;

import br.com.wine.api.dto.Cep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CEPRepository extends JpaRepository<Cep, Long> {
}
