package br.com.wine.api.repository;

import br.com.wine.api.dto.LojaDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LojaRepository extends JpaRepository<LojaDTO, Long> {

    LojaDTO findByCodigoLoja(String codigo);
}
