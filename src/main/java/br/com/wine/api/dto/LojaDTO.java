package br.com.wine.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "loja")
@JsonIgnoreProperties(value = { "id" })
public class LojaDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "CODIGO_LOJA")
    @JsonProperty("CODIGO_LOJA")
    private String codigoLoja;

    @Column(name = "FAIXA_INICIO")
    @JsonProperty("FAIXA_INICIO")
    private int faixaInicio;

    @Column(name = "FAIXA_FIM")
    @JsonProperty("FAIXA_FIM")
    private int faixaFim;

    public LojaDTO() {
    }

    public LojaDTO(Long id, String codigoLoja, int faixaInicio, int faixaFim) {
        this.id = id;
        this.codigoLoja = codigoLoja;
        this.faixaInicio = faixaInicio;
        this.faixaFim = faixaFim;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoLoja() {
        return codigoLoja;
    }

    public void setCodigoLoja(String codigoLoja) {
        this.codigoLoja = codigoLoja;
    }

    public int getFaixaInicio() {
        return faixaInicio;
    }

    public void setFaixaInicio(int faixaInicio) {
        this.faixaInicio = faixaInicio;
    }

    public int getFaixaFim() {
        return faixaFim;
    }

    public void setFaixaFim(int faixaFim) {
        this.faixaFim = faixaFim;
    }


}
