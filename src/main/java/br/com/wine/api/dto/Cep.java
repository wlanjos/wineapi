package br.com.wine.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "cep")
@JsonIgnoreProperties(value = { "id" })
public class Cep {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "tb_cep")
    private long cep;

    @ManyToOne
     @JoinColumn(name = "loja_id")
    private LojaDTO loja;

    public Cep() {
    }

    public Cep(Long id, long cep, LojaDTO loja) {
        this.id = id;
        this.cep = cep;
        this.loja = loja;
    }

    public LojaDTO getLoja() {
        return loja;
    }

    public void setLoja(LojaDTO loja) {
        this.loja = loja;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCep() {
        return cep;
    }

    public void setCep(long cep) {
        this.cep = cep;
    }
}
